
GST_PACKAGE = gst-package
GST_CONVERT = gst-convert

CONVERT_RULES = -r'Osmo.LogManager->LogManager' \
		-r'Osmo.LogArea->LogArea'  \
		-r'Osmo.LogLevel->LogLevel' \
		-r'DateTime->DateAndTime' \
		-r'(Duration milliseconds: ``@args1) -> (Duration milliSeconds: ``@args1)'
	

all:
	$(GST_PACKAGE) --test package.xml

convert:
	$(GST_CONVERT) $(CONVERT_RULES) -F squeak -f gst \
		-o fileout.st compat_for_pharo.st LogArea.st Dispatcher.st Timer.st Tests.st \
		changes_for_pharo.st
