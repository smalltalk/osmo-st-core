"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

TestCase subclass: DispatcherTest [
    <category: 'OsmoCore-Tests'>

    testDispatcher [
        | sem |
        sem := Semaphore new.

        self assert: sem signals = 0.
        "Force a debugger or some abortion? And a log message"
        OsmoDispatcher dispatchBlock: [123 unknownMessageForSmallInteger].
        OsmoDispatcher dispatchBlock: [Processor activeProcess terminate].
        OsmoDispatcher dispatchBlock: [sem signal].
        self assert: sem signals = 1.
    ]

    testSameDispatcher[
        self assert: Dispatcher instance == Dispatcher instance
    ]
]

TestCase subclass: TimerSchedulerTest [
    | timerScheduler |

    <category: 'OsmoCore-Tests'>

    tearDown [
        timerScheduler doShutDown
    ]

    setUp [
        timerScheduler := TimerScheduler new
    ]

    testTimer [
        | sem now |
        now := DateTime now.
        sem := Semaphore new.
        timerScheduler scheduleInSeconds: 2 block: [
            sem signal.
        ].

        sem wait.
        self assert: (DateTime now - now) asSeconds >= 2.
    ]

    testCancel [
        | timer1 timer2 fire1 sem block |
        sem := Semaphore new.
        block := [sem signal].


        fire1 := timerScheduler scheduleInSeconds: 5 block: block.
        timer1 := timerScheduler scheduleInSeconds: 3 block: block.
        timer2 := timerScheduler scheduleInSeconds: 2 block: block.
        timer2 cancel.
        timer1 cancel.

        sem wait.
        self assert: sem signals equals: 0
    ]
]
