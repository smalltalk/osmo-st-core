TimerScheduler extend [
    TimerScheduler class >> initialize [
        <category: 'loading'>
        "Pharo requires us to do some post-processing"
        Smalltalk addToStartUpList: self.
        Smalltalk addToShutDownList: self.
        ^self instance.
    ]

    TimerScheduler class >> startUp [
        Smalltalk at: #OsmoTimeScheduler ifPresent: [:timer | timer doStartUp].
    ]

    TimerScheduler class >> shutDown: quitting [
        Smalltalk at: #OsmoTimeScheduler ifPresent: [:timer | timer doShutDown].
    ]

    doShutDown [
        <category: 'PharoHacks'>
        loop ifNil: [^self].
        quit := true.
        sem critical: [
            loopSem ifNotNil: [loopSem signal]].
        delay ifNotNil: [:the_delay | the_delay signalWaitingProcess].
        processExited wait.
        Transcript
            nextPutAll: 'Stopped the TimerScheduler process';
            cr
    ]

    doStartUp [
        <category: 'PharoHacks'>
        loop ifNotNil: [^self error: 'The loop should have vanished'].
        Transcript nextPutAll: 'Starting the TimerScheduler loop again'; cr.
        quit := false.
        self startLoop.
    ]

    signalDelay [
        "Called with sem critical being consumed"
        delay ifNotNil: [delay signalWaitingProcess].
    ]

    scheduleInSeconds: aNumber block: aBlock [
        <category: 'schedule'>
        ^self scheduleIn: aNumber seconds block: aBlock
    ]
]

Dispatcher class extend [
    initialize [
        self instance
    ]
]
