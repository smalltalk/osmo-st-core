"
Test the Pharo extensions.
"

TestCase subclass: StringFormatTest [
    <category: 'OsmoCore-Pharo-Tests'>

    testExpandMacros [
        | str |
        str := 'Bla<n>' expandMacros.
        self assert: str = ('Bla', Character nl asString).

        str := 'Bla<t>' expandMacros.
        self assert: str = ('Bla', Character tab asString).
    ]

    testExpandWithMacro [
        | str |
        str := 'Bla Bla <1p>' expandMacrosWith: 10.
        self assert: str = 'Bla Bla 10'.

        str := 'Bla Bla <1s>' expandMacrosWith: 10 asString.
        self assert: str = 'Bla Bla 10'.
    ]

    testExpandMacrosTrue [
        | str |
        str := 'Should be true=<1?true:false>' expandMacrosWith: true.
        self assert: str = 'Should be true=true'.

        str := 'Should be false=<1?tue:false>' expandMacrosWith: false.
        self assert: str = 'Should be false=false'.
    ]

    testExpandMacros2 [
        | str |
        str := 'Bla Bla <1p> <2s>' expandMacrosWith: 10 with: '20'.
        self assert: str = 'Bla Bla 10 20'.
    ]
]
